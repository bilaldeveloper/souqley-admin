import { NODE_URL } from '../../config'

export const fetLogin = async (params) => {
    console.log("params =========================== ", params)
    let config = {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: `username=${params.username}&password=${params.password}`
    }

    try {
        const login = await fetch(`${NODE_URL}/api/sign-in`, config);
        console.log(login)
        return login

    } catch (error) {
        console.log(error)
    }
}