import { Map } from 'immutable';
import { getToken } from '../../helpers/utility';
import actions from './actions';

// const initState = new Map({
let initState = {
  idToken: null,
  isFetching: false,
  isAuthenticated: localStorage.getItem('id_token') ? true : false,
  error: "",
};
// });

const token = getToken()
initState = {
  idToken: token ? token : null,
  ...initState
}

// export default function authReducer(state = initState.merge(getToken()), action) {
function authReducer(state = initState, action) {
  // console.log("action.type", action.type)
  switch (action.type) {
    case actions.LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        creds: action.creds
      })
    case actions.LOGIN_SUCCESS:
      return state.set('idToken', action.token);
    case actions.LOGIN_ERROR:
      return state.set('error', action.message);
    case actions.LOGOUT:
      return initState;
    default:
      return state;
  }
}
export default authReducer
// import { Map } from 'immutable';
// import { getToken } from '../../helpers/utility';
// import actions from './actions';

// const initState = new Map({
//   idToken: 'secret token'
// });

// export default function authReducer(
//   state = initState.merge(getToken()),
//   action
// ) {
//   switch (action.type) {
//     case actions.LOGIN_SUCCESS:
//       return state.set('idToken', action.token);
//     case actions.LOGOUT:
//       return initState;
//     default:
//       return state;
//   }
// }