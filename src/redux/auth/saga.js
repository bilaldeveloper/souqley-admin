import { all, takeEvery, put, fork } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { clearToken } from '../../helpers/utility';
import actions from './actions';

const fakeApiCall = true; // auth0 or express JWT

export function* loginRequest(data) {
  yield takeEvery('LOGIN_REQUEST', function*(data) {
    let url = 'http://localhost:8000/api/sign-in'
    const resultApi = yield fetch(url,{
      method:'Post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(data.data)
        
    }).then(res=>res.json()).catch(error=>console.log(error))
    debugger
    
      if (resultApi.type == 'success') {
        yield put({
          type: actions.LOGIN_SUCCESS,
          token:resultApi.result.token,
          profile: 'Profile'
        });
      } else {
        yield put({ type: actions.LOGIN_ERROR });
      }
  
  });
}

export function* loginSuccess() {
  yield takeEvery(actions.LOGIN_SUCCESS, function*(payload) {
    debugger
    console.log(payload.token)
    yield localStorage.setItem('id_token', payload.token);
    
  });
}

export function* loginError() {
  yield takeEvery(actions.LOGIN_ERROR, function*() {

  });
}

export function* logout() {
  yield takeEvery(actions.LOGOUT, function*() {
    clearToken();
    yield put(push('/'));
  });
}
export default function* rootSaga() {
  yield all([
    fork(loginRequest),
    fork(loginSuccess),
    fork(loginError),
    fork(logout)
  ]);
}
