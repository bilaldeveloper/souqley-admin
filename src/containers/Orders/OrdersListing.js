import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Table, Tooltip, Popconfirm, Button, Icon, message, Modal } from 'antd';
import { rtl } from '../../config/withDirection';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/product/actions.js';

const {
  addTodo,
} = todoAction;

class OrdersListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      showOrder: false,
    };

    this.handleApprove = this.handleApprove.bind(this);
    this.handleReject = this.handleReject.bind(this);
  }
  handleApprove(e) {
    console.log(e)
    message.success('Order approved successfully.');
    this.setState({
      showOrder: false,
    });
  }
  handleReject(e) {
    console.log(e)
    message.success('Order rejected successfully.');
    this.setState({
      showOrder: false,
    });
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  showOrder = () => {
    this.setState({
      showOrder: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleOrder = e => {
    this.setState({
      showOrder: false,
    });
  };
  render() {

    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: 'Product',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Image',
        dataIndex: 'avatar',
        key: 'avatar',
        render: (text, record) => (
          <div size="middle">
            <img style={{
              width: '70px',
              height: '70px',
              backgroundSize: 'cover'
            }} src={record.avatar} />
          </div>
        ),
      },
      {
        title: 'SKU',
        dataIndex: 'sku',
        key: 'sku',
      },
      {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
        sorter: (a, b) => a.category.localeCompare(b.category),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <div size="middle">
            <Tooltip title="View Order">
              <Button type="default" onClick={this.showOrder}><Icon type="eye" theme="filled" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Accept Order">
              <Button type="primary" onClick={() => this.handleApprove(record.id)}><Icon type="check" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Recject Order">
              <Popconfirm title="Sure to reject?" onConfirm={() => this.handleReject(record.id)}>
                <Button type="danger"><Icon type="close" /></Button>
              </Popconfirm>
            </Tooltip>
          </div>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        id: '5',
        sku: 'JB001',
        name: 'Tommy Hill Figure Polo',
        category: 'Clothes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg',
      },
      {
        key: '2',
        id: '6',
        sku: 'GJ569',
        name: 'Reebok Casual Shoes',
        category: 'Shoes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/jnmnrd/128.jpg',
      },
      {
        key: '3',
        id: '8',
        sku: 'JB256',
        name: 'Apple Macbook Pro 11',
        category: 'Laptop',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/damenleeturks/128.jpg',
      },
      {
        key: '4',
        id: '9',
        sku: 'JD584',
        name: 'Acer Spin 3 1 TB RAM',
        category: 'Laptop',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/keryilmaz/128.jpg',
      },
      {
        key: '5',
        id: '11',
        sku: 'AP263',
        name: 'Samsumg S 20 Max',
        category: 'Mobile',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/romanbulah/128.jpg',
      },
      {
        key: '6',
        id: '13',
        sku: 'SG363',
        name: 'Samsung Bluetooth Headphone',
        category: 'Samsung',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/claudioguglieri/128.jpg',
      },
      {
        key: '7',
        id: '14',
        sku: 'CL444',
        name: 'Zara Henley Neck',
        category: 'Clothes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/128.jpg',
      },
      {
        key: '8',
        id: '15',
        sku: 'EL565',
        name: 'Gpas Trimmer',
        category: 'Electronics',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/okseanjay/128.jpg'
      },
      {
        key: '9',
        id: '17',
        sku: 'BK558',
        name: 'Harry Potter',
        category: 'Books',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/haligaliharun/128.jpg',
      },
      {
        key: '10',
        id: '18',
        sku: 'MB487',
        name: 'Nokia 6600',
        category: 'Mobiles',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/mhaligowski/128.jpg',
      },
      {
        key: '11',
        id: '20',
        sku: 'LP465',
        name: 'HP Laptop',
        category: 'Laptop',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg',
      },
      {
        key: '12',
        id: '21',
        sku: 'SE996',
        name: 'Kappa Shoes',
        category: 'Shoes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/mhaligowski/128.jpg',
      },
    ];

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px',

    };
    const colStyle = {
      marginBottom: '16px',
    };
    const gutter = 16;

    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };

    return (
      <LayoutWrapper>
        <PageHeader>
          List Orders
        </PageHeader>

        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Table columns={columns} dataSource={data} style={{ backgroundColor: '#fff' }} />
          </Col>
        </Row>

        <Modal
          title="Image"
          visible={this.state.visible}
          onOk={this.handleOk}
          footer={[
            <Button key="submit" type="default" onClick={this.handleOk}>
              Close
            </Button>,
          ]}
        >
          <img src="https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg" />
        </Modal>
        <Modal
          title="Order Details"
          visible={this.state.showOrder}
          onOk={this.handleOrder}
          footer={[
            <Button key="submit" type="default" onClick={this.handleOrder}>
              Close
            </Button>,
            <Button key="submit" type="primary" onClick={this.handleApprove}>
              Approve
            </Button>,
            <Button key="submit" type="danger" onClick={this.handleReject}>
              Reject
            </Button>,
          ]}
        >
          <p><strong>Product: </strong> Tommy Hill Figure Polo	</p>
          <p><strong>Quantity: </strong> 1 PC	</p>
          <p><strong>Price: </strong> AED 100.00</p>
          <p><strong>Payment Method: </strong> Cash on delivery</p>
          <p><strong>Payment Status: </strong> Not received</p>
          <br/>
          <img src="https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg" />
          <hr />
          <p><strong>Billing Details:</strong></p>

          <p>Mohammad Rashid</p>
          <p>901, AL Bayan, AL Qusais, Dubai</p>
          <p>Mobile: 0565520478</p>
          <p>Email: test@tester.com</p>

        </Modal>

      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(OrdersListing);