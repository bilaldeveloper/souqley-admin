import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Table, Tooltip, Popconfirm, Icon, Button, message, Modal } from 'antd';
import { rtl } from '../../config/withDirection';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/category/actions.js';

const {
  addTodo,
} = todoAction;

class ListBanner extends Component {
  constructor(props) {
    super(props);

    this.state = { visible: false };


    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(e) {
    console.log(e)
    message.success('Category deleted successfully');
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  render() {

    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: 'Title',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Image',
        dataIndex: 'avatar',
        key: 'avatar',
        render: (text, record) => (
          <div size="middle">
            <img style={{
              width: '70px',
              height: '70px',
              backgroundSize: 'cover'
            }} src={record.avatar} onClick={this.showModal} />
          </div>
        ),
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <div size="middle">
            <Tooltip title="Edit">
              <Button type="primary"><Icon type="edit" theme="filled" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Delete">
              <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                <Button type="danger"><Icon type="delete" theme="filled" /></Button>
              </Popconfirm>
            </Tooltip>
          </div>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        id: '5',
        name: 'John Brown',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg',
      },
      {
        key: '2',
        id: '6',
        name: 'Green Jim',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/jnmnrd/128.jpg',
      },
      {
        key: '3',
        id: '8',
        name: 'Joe Black',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/damenleeturks/128.jpg',
      },
      {
        key: '4',
        id: '9',
        name: 'John Doe',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/keryilmaz/128.jpg',
      },
      {
        key: '5',
        id: '11',
        name: 'Apple',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/romanbulah/128.jpg',
      },
      {
        key: '6',
        id: '13',
        name: 'Samsung',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/claudioguglieri/128.jpg',
      },
      {
        key: '7',
        id: '14',
        name: 'Clothes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/128.jpg',
      },
      {
        key: '8',
        id: '15',
        name: 'Electronics',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/okseanjay/128.jpg'
      },
      {
        key: '9',
        id: '17',
        name: 'Books',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/haligaliharun/128.jpg',
      },
      {
        key: '10',
        id: '18',
        name: 'Mobiles',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/mhaligowski/128.jpg',
      },
      {
        key: '11',
        id: '20',
        name: 'Laptop',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg',
      },
      {
        key: '12',
        id: '21',
        name: 'Shoes',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/mhaligowski/128.jpg',
      },
    ];

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px',

    };
    const colStyle = {
      marginBottom: '16px',
    };
    const gutter = 16;

    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };


    return (
      <LayoutWrapper>
        <PageHeader>
          List Banner
        </PageHeader>

        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Table columns={columns} dataSource={data} style={{ backgroundColor: '#fff' }} />
          </Col>
        </Row>

        <Modal
          title="Image"
          visible={this.state.visible}
          onOk={this.handleOk}
          footer={[
            <Button key="submit" type="default" onClick={this.handleOk}>
              Close
            </Button>,
          ]}
        >
          <img src="https://s3.amazonaws.com/uifaces/faces/twitter/bluefx_/128.jpg" />
        </Modal>

      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(ListBanner);
