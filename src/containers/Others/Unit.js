import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Icon, Button, Input, message, Tooltip, Popconfirm, Table } from 'antd';
import { RadioGroup } from '../../components/uielements/radio';
import { Textarea } from '../../components/uielements/input';
import { SelectOption } from '../../components/uielements/select';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { rtl } from '../../config/withDirection';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/category/actions.js';

const Option = SelectOption;

const {
  addTodo,
} = todoAction;

class Unit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      input: {
        name: "",
        product_type: 0,
        updated_by: "",
        isActive: 1
      }
    };
    this.onSubmitCategory = this.onSubmitCategory.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleChange = value => {
    this.setState({
      dataSource:
        !value || value.indexOf('@') >= 0
          ? []
          : [`${value}@gmail.com`, `${value}@163.com`, `${value}@qq.com`],
    });
  };
  onChange = e => {
    console.log(e.target.value)
    // this.setState({
    //   input[e.target.name]:e.target.value
    // })
  }
  onSubmitCategory = () => {
    message.success('Category added successfully');
  }

  // List Unit
  handleDelete(e) {
    console.log(e)
    message.success('Category deleted successfully');
  }


  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px'
    };
    const colStyle = {
      marginBottom: '16px',
    };
    const radioStyle = {
      display: 'block',
      marginBottom: '15px',
      marginTop: '15px',
    };
    const gutter = 16;
    const isActiveOptions = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 }
    ];
    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };

    const styleInput = {
      marginBottom: '15px'
    }

    const props = {
      name: 'file',
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      headers: {
        authorization: 'authorization-text',
      },
      onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };

    // List Unit
    
    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <div size="middle">
            <Tooltip title="Edit">
              <Button type="primary"><Icon type="edit" theme="filled" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Delete">
              <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                <Button type="danger"><Icon type="delete" theme="filled" /></Button>
              </Popconfirm>
            </Tooltip>
          </div>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        id: '5',
        name: 'KG',
      },
      {
        key: '2',
        id: '6',
        name: 'Unit',
      },
      {
        key: '3',
        id: '8',
        name: 'Litre',
      }
    ];

    const rowStyleList = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px',

    };

    return (
      <LayoutWrapper>
        <PageHeader>
          Add Unit
        </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={12} sm={12} xs={24} style={colStyle}>
            <Box title="Add New Unit">
              <ContentHolder>
                <Input placeholder="Name" name="name" onChange={this.onChange} style={styleInput} />
                <Textarea placeholder="Description" name="description" onChange={this.onChange} rows={6} style={styleInput} />

                <RadioGroup
                  style={radioStyle}
                  options={isActiveOptions}
                  onChange={this.onChange}
                  value={this.state.input.isActive}
                />
              </ContentHolder>



              <Button type="primary" style={margin} onClick={this.onSubmitCategory}>
                Submit
                </Button>
            </Box>
          </Col>
        </Row>

        <PageHeader>
          List Unit
        </PageHeader>

        <Row style={rowStyleList} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Table columns={columns} dataSource={data} style={{ backgroundColor: '#fff' }} />
          </Col>
        </Row>


      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(Unit);
