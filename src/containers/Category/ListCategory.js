import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Table, Tooltip, Popconfirm, Icon, Button, message } from 'antd';
import { rtl } from '../../config/withDirection';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/category/actions.js';

const {
  addTodo,
} = todoAction;

class ListCategory extends Component {
  constructor(props) {
    super(props);

    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(e) {
    console.log(e)
    message.success('Category deleted successfully');
  }
  render() {

    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: 'Category Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <div size="middle">
            <Tooltip title="Edit">
              <Button type="primary"><Icon type="edit" theme="filled" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Delete">
              <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                <Button type="danger"><Icon type="delete" theme="filled" /></Button>
              </Popconfirm>
            </Tooltip>
          </div>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        id: '5',
        name: 'John Brown',
      },
      {
        key: '2',
        id: '6',
        name: 'Green Jim',
      },
      {
        key: '3',
        id: '8',
        name: 'Joe Black',
      },
      {
        key: '4',
        id: '9',
        name: 'John Doe',
      },
      {
        key: '5',
        id: '11',
        name: 'Apple',
      },
      {
        key: '6',
        id: '13',
        name: 'Samsung',
      },
      {
        key: '7',
        id: '14',
        name: 'Clothes',
      },
      {
        key: '8',
        id: '15',
        name: 'Electronics',
      },
      {
        key: '9',
        id: '17',
        name: 'Books',
      },
      {
        key: '10',
        id: '18',
        name: 'Mobiles',
      },
      {
        key: '11',
        id: '20',
        name: 'Laptop',
      },
      {
        key: '12',
        id: '21',
        name: 'Shoes',
      },
    ];

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px',

    };
    const colStyle = {
      marginBottom: '16px',
    };
    const gutter = 16;

    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };

    return (
      <LayoutWrapper>
        <PageHeader>
          List Category
        </PageHeader>

        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Table columns={columns} dataSource={data} style={{ backgroundColor: '#fff' }} />
          </Col>
        </Row>

      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(ListCategory);
