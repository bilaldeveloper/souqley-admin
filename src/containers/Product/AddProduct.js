import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Icon, Input, Select, Button, message } from 'antd';
import { InputGroup } from '../../components/uielements/input';
import { RadioGroup } from '../../components/uielements/radio';
import { SelectOption } from '../../components/uielements/select';
import Box from '../../components/utility/box';
import ContentHolder from '../../components/utility/contentHolder';
import { rtl } from '../../config/withDirection';
import Async from '../../helpers/asyncComponent';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/product/actions.js';

const Editor = (props) => <Async load={import(/* webpackChunkName: "forms-editor" */ '../../components/uielements/editor')} componentProps={props} />;

const Option = SelectOption;

const {
  addTodo,
} = todoAction;

function uploadCallback(file) {
  return new Promise(
    (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://api.imgur.com/3/image');
      xhr.setRequestHeader('Authorization', 'Client-ID 8d26ccd12712fca');
      const data = new FormData();
      data.append('image', file);
      xhr.send(data);
      xhr.addEventListener('load', () => {
        const response = JSON.parse(xhr.responseText);
        resolve(response);
      });
      xhr.addEventListener('error', () => {
        const error = JSON.parse(xhr.responseText);
        reject(error);
      });
    }
  );
}

class AddProduct extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      input: {
        name: "",
        product_type: 0,
        updated_by: "",
        isActive: 1
      },
      editorState: null,
      loading: false,
      iconLoading: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange = value => {
    this.setState({
      dataSource:
        !value || value.indexOf('@') >= 0
          ? []
          : [`${value}@gmail.com`, `${value}@163.com`, `${value}@qq.com`],
    });
  };
  onChange = e => {
    console.log(e.target.value)
    // this.setState({
    //   input[e.target.name]:e.target.value
    // })
  }
  onSubmit = () => {
    message.success('Product added successfully');
  }
  render() {
    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
    };
    const colStyle = {
      marginBottom: '16px',
    };
    const radioStyle = {
      display: 'block',
      marginBottom: '15px',
      marginTop: '15px',
    };
    const gutter = 16;
    const isActiveOptions = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 }
    ];
    console.log(rtl)
    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };
    const onEditorStateChange = (editorState) => {
      this.setState({ editorState });
    }
    const editorOption = {
      style: { width: '90%', height: '70%', marginTop: '15px', marginBottom: '15px' },
      editorState: this.state.editorState,
      toolbarClassName: 'home-toolbar',
      wrapperClassName: 'home-wrapper',
      editorClassName: 'home-editor',
      onEditorStateChange: onEditorStateChange,
      uploadCallback: uploadCallback,
      toolbar: { image: { uploadCallback: uploadCallback } },
    };
    const inputText = {
      style: {
        width: '49%',
        marginBottom: '10px',
        marginTop: '10px',
        float: 'left',
        ':nth-child(odd)': {
          marginRight: '15px',
        }
      }
    }
    return (
      <LayoutWrapper>
        <PageHeader>
          Add Product
        </PageHeader>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={18} sm={18} xs={24} style={colStyle}>
            <Box title="Add New Product">
              <ContentHolder>
                <InputGroup compact style={{ marginBottom: '15px', marginTop: '15px' }}>
                  <Select defaultValue="0" style={{ width: '48%' }} name="product_type">
                    <Option value="0" selected>Select Unit</Option>
                    <Option value="Unit">Unit</Option>
                    <Option value="KG">KG</Option>
                    <Option value="Litre">Litre</Option>
                  </Select>
                  <Select defaultValue="0" style={{ width: '49%', marginLeft: '15px' }} name="product_type">
                    <Option value="0" selected>Select Variant</Option>
                    <Option value="Color">Color</Option>
                    <Option value="Size">Size</Option>
                    <Option value="RAM">RAM</Option>
                  </Select>

                </InputGroup>

                <InputGroup compact style={{ marginBottom: '15px', marginTop: '15px' }}>
                  <Select defaultValue="0" style={{ width: '48%' }} name="product_type">
                    <Option value="0" selected>Select Currency</Option>
                    <Option value="INR">INR</Option>
                    <Option value="AED">AED</Option>
                    <Option value="USD">USD</Option>
                  </Select>
                </InputGroup>


                <InputGroup compact style={{ marginBottom: '15px', marginTop: '15px' }}>
                  <Select defaultValue="0" style={{ width: '48%' }} name="product_type">
                    <Option value="0" selected>Select Sub Category</Option>
                    <Option value="Clothes">Clothes</Option>
                    <Option value="Shoes">Shoes</Option>
                    <Option value="Electronics">Electronics</Option>
                  </Select>
                  <Select defaultValue="0" style={{ width: '49%', marginLeft: '15px' }} name="product_type">
                    <Option value="0" selected>Select Seller Sub Category</Option>
                    <Option value="Mobile Company ABC">Mobile Company ABC</Option>
                    <Option value="Laotop Company XYZ">Laotop Company XYZ</Option>
                    <Option value="Seller 1">Seller 1</Option>
                    <Option value="Seller 2">Seller 2</Option>
                  </Select>
                </InputGroup>
                <InputGroup compact style={{ marginBottom: '15px', marginTop: '15px' }}>
                </InputGroup>


                <Input placeholder="Product Name" name="Product_Name" onChange={this.onChange} style={{ marginBottom: '15px' }} />

                <Editor{...editorOption} />

                <Row style={{ marginTop: '15px', marginBottom: '15px' }} gutter={16}>
                  <Col span={12}>
                    <Input placeholder="Price" name="Price" onChange={this.onChange} /></Col>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="SKU" name="SKU" onChange={this.onChange} /></Col>
                </Row>
                <Row style={{ marginTop: '15px' }} gutter={16}>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Availability" name="Availability_Count" onChange={this.onChange} /></Col>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Discount Percentage" name="Percentage_Discount" onChange={this.onChange} /></Col>
                </Row>
                <Row style={{ marginTop: '15px' }} gutter={16}>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Special Offer Price" name="Special_Offer_Price" onChange={this.onChange} /></Col>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Special Offer Minimum Quantity" name="Special_Offer_Minimum_Quantity" onChange={this.onChange} /></Col>
                </Row>
                <Row style={{ marginTop: '15px' }} gutter={16}>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Special Offer Maximum Quantity" name="Special_Offer_Maximum_Quantity" onChange={this.onChange} />
                  </Col>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Special Offer Discount Factor" name="Special_Offer_Discount_Factor" onChange={this.onChange} />
                  </Col>
                </Row>
                <Row style={{ marginTop: '15px' }} gutter={16}>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Minimum Allowed Buy Quantity" name="Minimum_Allowed_Buy_Quantity" onChange={this.onChange} />
                  </Col>
                  <Col span={12} style={{ marginBottom: '15px' }}>
                    <Input placeholder="Maximum Allowed Buy Quantity" name="Maximum_Allowed_Buy_Quantity" onChange={this.onChange} />
                  </Col>
                </Row>

                <RadioGroup
                  style={radioStyle}
                  options={isActiveOptions}
                  onChange={this.onChange}
                  value={this.state.input.isActive}
                />
              </ContentHolder>

              <Button type="primary" style={margin} onClick={this.onSubmit}>
                Submit
                </Button>
            </Box>
          </Col>
        </Row>
      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(AddProduct);
