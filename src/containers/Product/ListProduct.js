import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Col, Row, Table, Tooltip, Popconfirm, Button, Icon, message } from 'antd';
import { rtl } from '../../config/withDirection';

import PageHeader from '../../components/utility/pageHeader';
import LayoutWrapper from '../../components/utility/layoutWrapper';
import todoAction from '../../redux/product/actions.js';

const {
  addTodo,
} = todoAction;

class ListProduct extends Component {
  constructor(props) {
    super(props);

    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(e) {
    console.log(e)
    message.success('Product deleted successfully');
  }
  render() {

    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: 'Product Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'SKU',
        dataIndex: 'sku',
        key: 'sku',
      },
      {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
        sorter: (a, b) => a.category.localeCompare(b.category),
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <div size="middle">
            <Tooltip title="Edit">
              <Button type="primary"><Icon type="edit" theme="filled" /></Button>
            </Tooltip> &nbsp;&nbsp;
            <Tooltip title="Delete">
              <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                <Button type="danger"><Icon type="delete" theme="filled" /></Button>
              </Popconfirm>
            </Tooltip>
          </div>
        ),
      },
    ];

    const data = [
      {
        key: '1',
        id: '5',
        sku: 'JB001',
        name: 'Tommy Hill Figure Polo',
        category: 'Clothes',
      },
      {
        key: '2',
        id: '6',
        sku: 'GJ569',
        name: 'Reebok Casual Shoes',
        category: 'Shoes',
      },
      {
        key: '3',
        id: '8',
        sku: 'JB256',
        name: 'Apple Macbook Pro 11',
        category: 'Laptop',
      },
      {
        key: '4',
        id: '9',
        sku: 'JD584',
        name: 'Acer Spin 3 1 TB RAM',
        category: 'Laptop',
      },
      {
        key: '5',
        id: '11',
        sku: 'AP263',
        name: 'Samsumg S 20 Max',
        category: 'Mobile',
      },
      {
        key: '6',
        id: '13',
        sku: 'SG363',
        name: 'Samsung Bluetooth Headphone',
        category: 'Samsung',
      },
      {
        key: '7',
        id: '14',
        sku: 'CL444',
        name: 'Zara Henley Neck',
        category: 'Clothes',
      },
      {
        key: '8',
        id: '15',
        sku: 'EL565',
        name: 'Gpas Trimmer',
        category: 'Electronics',
      },
      {
        key: '9',
        id: '17',
        sku: 'BK558',
        name: 'Harry Potter',
        category: 'Books',
      },
      {
        key: '10',
        id: '18',
        sku: 'MB487',
        name: 'Nokia 6600',
        category: 'Mobiles',
      },
      {
        key: '11',
        id: '20',
        sku: 'LP465',
        name: 'HP Laptop',
        category: 'Laptop',
      },
      {
        key: '12',
        id: '21',
        sku: 'SE996',
        name: 'Kappa Shoes',
        category: 'Shoes',
      },
    ];

    const rowStyle = {
      width: '100%',
      display: 'flex',
      flexFlow: 'row wrap',
      marginBottom: '15px',

    };
    const colStyle = {
      marginBottom: '16px',
    };
    const gutter = 16;

    const margin = {
      margin: rtl === 'rtl' ? '0 0 8px 8px' : '15px 8px 8px 0'
    };

    return (
      <LayoutWrapper>
        <PageHeader>
          List Product
        </PageHeader>

        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <Table columns={columns} dataSource={data} style={{ backgroundColor: '#fff' }} />
          </Col>
        </Row>

      </LayoutWrapper>
    )
  }
}

function mapStateToProps(state) {
  const { todos, colors } = state.Todos.toJS();
  return {
    todos,
    colors,
  };
}
export default connect(mapStateToProps, {
  addTodo,
})(ListProduct);