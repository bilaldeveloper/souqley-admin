import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Input from '../../components/uielements/input';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import Auth0 from '../../helpers/auth0/index';
import Firebase from '../../helpers/firebase';
import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignUpStyleWrapper from './signup.style';

const { login } = authAction;

class SignUp extends React.Component {
  state = {
    redirectToReferrer: false,
    firstName: '',
    lastName: '',
    userName: '',
    email: '',
    password: '',
    confirmPassword: ''

  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  handleLogin = () => {
    const { login } = this.props;
    login();
    this.props.history.push('/dashboard');
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value

    })
  }
  handleSignup = () => {
    let data = {
      "first_name": this.state.firstName,
      "last_name": this.state.lastName,
      "password": this.state.password,
      "email": this.state.email,
      "username": this.state.userName,
    }
    if(this.state.password === this.state.confirmPassword){

      let url = 'http://localhost:8000/api/create-account'

      fetch(url, {
      method: 'Post',
      body: JSON.stringify(data),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
     
    }).then(res => res.json()).then(res => console.log(res)).catch(error => console.log(error))
    }else{

      console.log('wrong')
    }

  }
  render() {
    return (
      <SignUpStyleWrapper className="isoSignUpPage">
        <div className="isoSignUpContentWrapper">
          <div className="isoSignUpContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signUpTitle" />
              </Link>
            </div>

            <div className="isoSignUpForm">
              <div className="isoInputWrapper isoLeftRightComponent">
                <Input size="large" placeholder="First name" name="firstName" onChange={this.handleChange} />
                <Input size="large" placeholder="Last name" name="lastName" onChange={this.handleChange} />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" placeholder="Username" name="userName" onChange={this.handleChange} />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" placeholder="Email" name="email" onChange={this.handleChange} />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" type="password" placeholder="Password" name="password" onChange={this.handleChange} />
              </div>

              <div className="isoInputWrapper">
                <Input
                  size="large"
                  type="password"
                  placeholder="Confirm Password"
                  name="confirmPassword"
                  onChange={this.handleChange}
                />
              </div>

              <div className="isoInputWrapper" style={{ marginBottom: '50px' }}>
                <Checkbox>
                  <IntlMessages id="page.signUpTermsConditions" />
                </Checkbox>
              </div>
              <div className="isoInputWrapper">
                <p className="error"></p>
              </div>
              <div className="isoInputWrapper">

                <Button type="primary" onClick={this.handleSignup}>
                  <IntlMessages id="page.signUpButton" />
                </Button>
              </div>
              <div className="isoInputWrapper isoOtherLogin">
                <Button onClick={this.handleLogin} type="primary btnFacebook">
                  <IntlMessages id="page.signUpFacebook" />
                </Button>
                <Button onClick={this.handleLogin} type="primary btnGooglePlus">
                  <IntlMessages id="page.signUpGooglePlus" />
                </Button>
                {Auth0.isValid &&
                  <Button
                    onClick={() => {
                      Auth0.login(this.handleLogin);
                    }}
                    type="primary btnAuthZero"
                  >
                    <IntlMessages id="page.signUpAuth0" />
                  </Button>}

                {Firebase.isValid &&
                  <FirebaseLogin signup={true} login={this.handleLogin} />}
              </div>
              <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                <Link to="/signin">
                  <IntlMessages id="page.signUpAlreadyAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignUpStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.idToken !== null ? true : false,
  }),
  { login }
)(SignUp);
